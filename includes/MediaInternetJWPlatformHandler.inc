<?php

/**
 * Implementation of MediaInternetBaseHandler.
 *
 * @see hook_media_internet_providers().
 */
class MediaInternetJWPlatformHandler extends MediaInternetBaseHandler {
  public function parse($embedCode) {
    preg_match('@content\.jwplatform\.com/players/([\w-]+)\.js@i', $embedCode, $matches);
    if (isset($matches[1])) {
      return file_stream_wrapper_uri_normalize('jwplatform://v/' . $matches[1]);
    }
  }

  public function claim($embedCode) {
    if ($this->parse($embedCode)) {
      return TRUE;
    }
  }

  public function getFileObject() {
    $uri = $this->parse($this->embedCode);
    $file = file_uri_to_object($uri, TRUE);

    // Try to default the file name to the video's title.
    // TODO (Can use http://apidocs.jwplatform.com/methods/videos/show.html)
    if (empty($file->fid)) {
      //$file->filename = truncate_utf8($title, 255);
      $file->filename = 'A JW Platform video';
    }

    return $file;
  }
}

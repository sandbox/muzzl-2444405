<?php

/**
 * @file media_jw_platform/includes/themes/media_jw_platform.theme.inc
 *
 * Theme and preprocess functions for Media: JW Player.
 */

/**
 * Preprocess function for theme('media_jw_platform_video').
 */
function media_jw_platform_preprocess_media_jw_platform_video(&$variables) {
  // Build the URI.
  $wrapper = file_stream_wrapper_get_instance_by_uri($variables['uri']);
  $parts = $wrapper->get_parameters();
  $variables['video_id'] = $parts['v'];
}

<?php

/**
 * @file media_jw_platform/includes/themes/media-jw-platform-video.tpl.php
 *
 * Template file for theme('media_jw_platform_video').
 *
 * Variables available:
 *  $video_id - The unique identifier of the JW Platform video and player.
 */

?>
<div class="<?php print $classes; ?> media-jwplatform-<?php print $id; ?>">
  <script type="text/javascript" src="http://content.jwplatform.com/players/<?php print $video_id; ?>.js"></script>
</div>

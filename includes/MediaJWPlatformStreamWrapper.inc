<?php

/**
 *  @file
 *  Create a JWPlatform Stream Wrapper class for the Media/Resource module.
 */

/**
 *  Create an instance like this:
 *  $jwplatform = new ResourceJWPlatformStreamWrapper('jwplatform://v/[video-code]');
 */
class MediaJWPlatformStreamWrapper extends MediaReadOnlyStreamWrapper {
  static function getMimeType($uri, $mapping = NULL) {
    return 'video/jwplatform';
  }

  function getTarget($f) {
    return FALSE;
  }

  function interpolateUrl() {
    return 'http://content.jwplatform.com/players/' . $this->parameters['v'] . '.js';
  }

  function getLocalThumbnailPath() {
    $local_path = 'public://media-jw-platform/' . $this->parameters['v'] . '.jpg';
    if (!file_exists($local_path)) {
      $dirname = drupal_dirname($local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      $video_key = preg_replace('/-.*$/', '', $this->parameters['v']);
      @copy("http://content.jwplatform.com/thumbs/$video_key-320.jpg", $local_path);
    }
    return $local_path;
  }
}


Media: JW Platform

Creates a JW Platform PHP Stream Wrapper for Resource and implements the various
formatter and file listing hooks in the Media module.

Derived from Media: Vimeo

================================================================================

Installation:

Enable the module and set up the file display settings for Preview and WYSIWYG
view modes.

================================================================================

Usage:

Upload a video to your JW platform account, open the embed code for it making
sure the correct player is chosen, then copy and paste the embed code in the
text box in the web tab of the media browser.

It should work as long as the URL part is present.  Ex:

http://content.jwplatform.com/players/zzzzzzzz-yyyyyyyy.js

================================================================================

TODO

- Set up some default file display settings.
- Use API to determine video title and possibly description.
- Allow users to upload videos to the platform from Drupal.
